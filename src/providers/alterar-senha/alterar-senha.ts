import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { SolicitarAutenticarProvider } from "../solicitar-autenticar/solicitar-autenticar";

/*
  Generated class for the AlterarSenhaProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class AlterarSenhaProvider {

  id_token: string;
  headers: Headers;
  constructor(
    public http: Http,
    public sarProvider: SolicitarAutenticarProvider

  ) {
    this.id_token = this.sarProvider.getToken();
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', 'Bearer ' + this.id_token);
  }

  alterarSenha(data: any): Observable<any> {
    let option = new RequestOptions({ headers: this.headers });
    return this.http.post('http://hackathonapi.sefaz.al.gov.br/sfz-nfcidada-api/api/public/usuario/alterarSenha', data, option).map(res=> res.json());
  }

}
