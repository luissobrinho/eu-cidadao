import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { NativeStorage } from '@ionic-native/native-storage';
import 'rxjs/add/operator/map';

import { SolicitarAutenticar } from "../../models/solicitarAutenticar.model";
import { AutenticacaoAutenticar } from "../../models/autenticacaoAutenticar.model";

@Injectable()
export class SolicitarAutenticarProvider {

  solicitarAutenticar: SolicitarAutenticar = null;


  constructor(
    public http: Http,
    public storage: NativeStorage
  ) {


  }

  solicitar(sa: SolicitarAutenticar): Promise<any> {
    return new Promise<any>((resolver, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      let options = new RequestOptions({ headers: headers });
      this.http
        .post('http://hackathonapi.sefaz.al.gov.br/sfz-habilitacao-aplicativo-api/api/public/autorizacao-aplicativo/solicitar',
        {
          'login': sa.login,
          'nomeDispositivo': sa.nomeDispositivo || 'test',
          'tokenApp': sa.tokenApp
        },
        options
        )
        .map(res => res.json())
        .subscribe((data) => {
          resolver(data);
        },
        (e) => {
          reject(e)
        });
    });
  }

  autenticacao(aa: AutenticacaoAutenticar): Promise<any> {
    console.log('this.autenticacao', aa);

    return new Promise<any>((resolver, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      let options = new RequestOptions({ headers: headers });
      this.http
        .post('http://hackathonapi.sefaz.al.gov.br/api/public/autenticar',
        {
          'login': aa.login,
          'idAutorizacao': aa.idAutorizacao,
          'tokenApp': aa.tokenApp
        },
        options
        )
        .map(res => res.json())
        .subscribe((data) => {
          resolver(data);
        },
        (e) => {
          reject(e)
        });
    });
  }

  saveIdAutenticacao(object: { idAutorizacao: string, urlAutorizacao: string }, cpf: string) {
    window.localStorage.setItem('idAutorizacao', object.idAutorizacao);
    window.localStorage.setItem('cpf', cpf);
  }

  getIdAutenticacao(): string {
    return window.localStorage.getItem('idAutorizacao');
  }

  setToken(id_token: string) {
    window.localStorage.setItem('id_token', id_token);
  }

  getToken(): string {
    return window.localStorage.getItem('id_token');
  }

  getCpf(): string {
    return window.localStorage.getItem('cpf');
  }

}
