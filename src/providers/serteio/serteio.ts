import { Observable } from 'rxjs/Observable';
import { Sorteio } from './../../models/sorteio.model';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { SolicitarAutenticarProvider } from "../solicitar-autenticar/solicitar-autenticar";

/*
  Generated class for the SerteioProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class SerteioProvider {
  id_token: string;
  headers: Headers;
  constructor(
    public http: Http,
    public sarProvider: SolicitarAutenticarProvider,
  ) {

  }
  getSorteio(): Observable<any> {
    this.id_token = this.sarProvider.getToken();
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', 'Bearer ' + this.id_token);

    let option = new RequestOptions({ headers: this.headers });
    return this.http.get('http://hackathonapi.sefaz.al.gov.br/sfz-nfcidada-api/api/public/sorteio',option)
      .map((res) => res.json());
  }
}
