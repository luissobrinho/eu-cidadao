import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { SolicitarAutenticarProvider } from "../solicitar-autenticar/solicitar-autenticar";

/*
  Generated class for the BilheteProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class BilheteProvider {
  id_token: string;
  headers: Headers;
  constructor(
    public http: Http,
    public sarProvider: SolicitarAutenticarProvider
  ) {
    
  }

  getSorteio(data: {sequencialSorteio: string, documento: string}): Observable<any> {
    this.id_token = this.sarProvider.getToken();
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', 'Bearer ' + this.id_token);

    let option = new RequestOptions({ headers: this.headers });
    return this.http.post('http://hackathonapi.sefaz.al.gov.br/sfz-nfcidada-api/api/public/bilhete', data ,option)
      .map((res) => res.json());
  }

  getBilhetePremiado(data: {documento: string, sequencialSorteio: number}): Observable<any> {
    this.id_token = this.sarProvider.getToken();
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', 'Bearer ' + this.id_token);
    let option = new RequestOptions({ headers: this.headers });

    return this.http.post('http://hackathonapi.sefaz.al.gov.br/sfz-nfcidada-api/api/public/bilheteContemplado', data ,option)
    .map((res) => res.json());
  }
}
