import { SolicitarAutenticarProvider } from './../solicitar-autenticar/solicitar-autenticar';
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from "rxjs/Observable";
import { Nota } from "../../models/nota.model";

@Injectable()
export class NotaProvider {
  id_token: string;
  headers: Headers;
  constructor(
    public http: Http,
    public sarProvider: SolicitarAutenticarProvider
  ) {
    this.id_token = this.sarProvider.getToken();
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', 'Bearer ' + this.id_token);
    
  }

  getNotas(data: { dataCompetencia: string, numeroDestinatario: string }): Observable<Nota[]> {
    let option = new RequestOptions({ headers: this.headers });
    return this.http
      .post('http://hackathonapi.sefaz.al.gov.br/sfz-nfcidada-api/api/public/notas',
      data ,
      option)
      .map((res) => res.json());

  }

}
