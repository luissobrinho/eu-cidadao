import { App } from 'ionic-angular';
import { Component, Input } from '@angular/core';
import { BaseComponent } from "../base.component";

/**
 * Generated class for the HeaderAppComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'header-app',
  templateUrl: 'header-app.html'
})
export class HeaderAppComponent extends BaseComponent {

  @Input('title') text: string;
  @Input() color: string = '';

  constructor(
    public app: App
  ) {
    super(app);
    console.log('Hello HeaderAppComponent Component');
    this.text = 'Hello World';
  }
}
