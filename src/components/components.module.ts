import { NgModule } from '@angular/core';
import { HeaderAppComponent } from './header-app/header-app';
@NgModule({
	declarations: [HeaderAppComponent],
	imports: [],
	exports: [HeaderAppComponent]
})
export class ComponentsModule {}
