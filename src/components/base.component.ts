import { NotificacaoPage } from './../pages/notificacao/notificacao';
import { NavController, App } from 'ionic-angular';
import { OnInit } from "@angular/core";
export abstract class BaseComponent implements OnInit {

    protected navCtrl: NavController
    constructor(
        public app: App
    ) {

    }

    ngOnInit(): void {
        this.navCtrl = this.app.getActiveNav();
    }


    openNotificacao() {
        this.navCtrl.push(NotificacaoPage)
    }
}