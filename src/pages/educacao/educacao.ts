import { StatusBar } from '@ionic-native/status-bar';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-educacao',
  templateUrl: 'educacao.html',
})
export class EducacaoPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public statusBar: StatusBar
  ) {
  }

  ionViewDidEnter() {
    this.statusBar.backgroundColorByHexString('#000000');
  }

}
