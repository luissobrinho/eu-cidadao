import { GamePage } from './../game/game';
import { CreditoPage } from './../credito/credito';
import { EducacaoPage } from './../educacao/educacao';
import { DenunciaPage } from './../denuncia/denuncia';
import { NotificacaoPage } from './../notificacao/notificacao';
import { AlterarSenhaPage } from './../alterar-senha/alterar-senha';
import { ConsultaPage } from './../consulta/consulta';
import { StatusBar } from '@ionic-native/status-bar';
import { Component } from '@angular/core';
import { NavController, AlertController, MenuController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    public navCtrl: NavController,
    public statusBar: StatusBar,
    public arlertCtrl: AlertController,
    public menuCtrl: MenuController
  ) {

  }

  ionViewDidEnter() {
    this.statusBar.backgroundColorByHexString('#757575');
    this.menuCtrl.enable(true, 'menu');
  }

  openAlterarSenha() {
    this.navCtrl.push(AlterarSenhaPage)
  }

  openConsulta() {
    this.navCtrl.push(ConsultaPage);
  }
  openNofiticacao() {
    this.navCtrl.push(NotificacaoPage);
  }

  openIncluirDenuncia() {
    this.navCtrl.push(DenunciaPage);
  }

  openCredito() {
    this.navCtrl.push(CreditoPage)
  }

  openGame() {
    this.navCtrl.push(GamePage);
  }

  openEducacao() {
    this.navCtrl.push(EducacaoPage)
  }
}
