import { AngularFire, FirebaseAuthState } from 'angularfire2';
import { StatusBar } from '@ionic-native/status-bar';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PerfilPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {
  user: firebase.UserInfo;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public statusBar: StatusBar
  ) {
    this.user = this.navParams.get('user');
    
  }

  ionViewDidEnter() {
    this.statusBar.backgroundColorByHexString('#388E3C');
  }

}
