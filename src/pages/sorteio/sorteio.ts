import { SolicitarAutenticarProvider } from './../../providers/solicitar-autenticar/solicitar-autenticar';
import { BilheteProvider } from './../../providers/bilhete/bilhete';
import { SerteioProvider } from './../../providers/serteio/serteio';
import { StatusBar } from '@ionic-native/status-bar';
import { Sorteio } from './../../models/sorteio.model';
import { PopoverConfigSorteio } from './popoverConfigSorteio';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, Popover, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { Bilhete } from "../../models/bilhete.model";
import { BilhetePremiado } from "../../models/bilhetePremiado.model";

/**
 * Generated class for the SorteioPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sorteio',
  templateUrl: 'sorteio.html',
})
export class SorteioPage {
  sorteios: Sorteio[] = [];
  sorteiocurrent: Sorteio;
  bilhetes: Bilhete[] = []
  constructor(
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public statusBar: StatusBar,
    public sProvider: SerteioProvider,
    public bProvider: BilheteProvider,
    public saProvider: SolicitarAutenticarProvider,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController
  ) {
    this.sorteiocurrent = {
      codigoSorteio: 0,
      descricao: '',
      dataRealizacao: '',
      sequencial: 0
    }
    let os = this.sProvider.getSorteio();
    os.subscribe((sorteio: Sorteio[]) => {
      this.sorteios = sorteio.reverse();
      this.sorteios.forEach((sorteio, index) => {
        if (index === 0) {
          this.sorteiocurrent = sorteio
          this.bProvider
          .getSorteio({"sequencialSorteio": '37',"documento": "46898042491"})
          .subscribe((bilhetes: Bilhete[]) => {
            this.bilhetes = bilhetes;
            console.log(this.bilhetes);
          })
        }
      })
    });
  }

  ionViewDidEnter() {
    this.statusBar.backgroundColorByHexString('#1976D2');
  }
  popoverConfigSorteio(event) {
    let popover: Popover;
    popover = this.popoverCtrl.create(PopoverConfigSorteio, { sorteios: this.sorteios });

    popover.present({
      ev: event
    });
    popover.onDidDismiss((data) => {
      this.sorteiocurrent = data || this.sorteiocurrent;
    })
  }

  bilhetePremiado() {
    this.bProvider
    .getBilhetePremiado({"documento": "03918819493","sequencialSorteio": 37})
    .subscribe((bilhetes: BilhetePremiado[]) => {
      if(bilhetes.length === 0) {
        this.toastCtrl.create({
          message: 'Não há biletes premiado para você!',
          position: 'button',
          duration: 3000
        }).present();
      } else {
        bilhetes.forEach((bilhete: BilhetePremiado) => {
          this.loadingCtrl.create({
            content:  `O bilhete nº: ${bilhete.bilhete.numeroBilhete}, foi premiado no valor de ${bilhete.valorPremio} reais.`,
            duration: 3500,
            spinner: 'hide',
          }).present();
        })
      }
    })
  }
}
