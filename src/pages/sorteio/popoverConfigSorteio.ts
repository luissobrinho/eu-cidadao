import { NavController, NavParams, ViewController } from 'ionic-angular';
import { Component } from '@angular/core';
import { Sorteio } from './../../models/sorteio.model';


@Component({
    selector: 'popover-config-sorteio',
    template: `
    <ion-list>
        <ion-item *ngFor="let sorteio of sorteios" (click)="changerSorteio(sorteio)">
            {{sorteio.sequencial}} - {{sorteio.dataRealizacao | date:'dd/MM/y'}}
        </ion-item>
    </ion-list>
    `
})
export class PopoverConfigSorteio {

    sorteios: Sorteio[];
    sorteio: any;
    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public viewCtrl: ViewController
    ) {
        this.sorteios = this.navParams.data.sorteios;
    }

    changerSorteio(sorteio) {
        this.sorteio = sorteio;
        console.log(sorteio);
        this.viewCtrl.dismiss(sorteio);
        
    }
}