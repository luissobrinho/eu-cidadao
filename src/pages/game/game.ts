import { RankingPage } from './../ranking/ranking';
import { StatusBar } from '@ionic-native/status-bar';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import * as firebase from 'firebase/app';
import { FirebaseAuthState, AngularFire } from "angularfire2";



@IonicPage()
@Component({
  selector: 'page-game',
  templateUrl: 'game.html',
})
export class GamePage {
  vezes: number = 0;
  is: boolean = true;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private af: AngularFire,
    public statusBar: StatusBar,
    public loadingCtrl: LoadingController
  ) {
    this.af.auth.subscribe((state: FirebaseAuthState) => {
      this.is = (state === null)
    });
  }

  ionViewDidEnter() {
    this.statusBar.backgroundColorByHexString('#388E3C');
  }

  signInWithGoogle() {
    var provider = new firebase.auth.GoogleAuthProvider();

    firebase.auth().signInWithPopup(provider).then((result) => {
      if (result.credential) {
        // This gives you a Google Access Token.
        // You can use it to access the Google API.
        var token = result.credential.accessToken;
        // The signed-in user info.
        var user = result.user;
        // ...
        console.log(user);

      }
    }).catch((error: any) => {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
    });

  }

  openRankings() {
    this.navCtrl.push(RankingPage);
  }

  get logado(): boolean {
    let loading = this.loadingCtrl.create({
      content: 'Aguarde...',
      duration: 1500
    });
    if (this.vezes === 0) {
      loading.present();
      this.vezes++;
    }
    return this.is;
  }

}
