import { NavParams } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
    selector: 'popover-credito',
    template: `
    <ion-list>
      <ion-item>
          Total Crédito: {{credito | currency:'BRL':true}}
      </ion-item>
    </ion-list>
    `
  })
  export class PopoverCredito {
    credito: number = 0;

    constructor(navParams: NavParams) {
      this.credito = navParams.data.credito
    }
  }