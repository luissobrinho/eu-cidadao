import { Observable } from 'rxjs/Observable';
import { Nota } from './../../models/nota.model';
import { SolicitarAutenticarProvider } from './../../providers/solicitar-autenticar/solicitar-autenticar';
import { NotaProvider } from './../../providers/nota/nota';
import { PopoverCredito } from './popoverCredito';
import { StatusBar } from '@ionic-native/status-bar';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, AlertController } from 'ionic-angular';
import { ConsultaCreditoProvider } from "../../providers/consulta-credito/consulta-credito";

@IonicPage()
@Component({
  selector: 'page-notas',
  templateUrl: 'notas.html',
})
export class NotasPage {
  notas: Observable<Nota[]>;
  vCredito: number = 0;
  mes: {valorTotal: number, name: string}
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public statusBar: StatusBar,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public notaProvider: NotaProvider,
    public saProvider: SolicitarAutenticarProvider,
    public csProvider: ConsultaCreditoProvider
  ) {
    this.csProvider.getCredito()
      .subscribe((valor) => {
        this.vCredito = valor.valorCredito;
      });
      this.mes = {valorTotal: 0, name: '03/2017'};
  }

  ionViewDidEnter() {
    this.statusBar.backgroundColorByHexString('#1976D2');
    this.notas = this.notaProvider
      .getNotas({ dataCompetencia: '201603', numeroDestinatario: "12270435000135" })
    this.notas.subscribe((notas: Nota[]) => {
      notas.forEach((nota: Nota) => {
        this.mes.valorTotal += parseFloat(nota.valorTotal);
      })
    })

    
  }

  popoverCredito(event) {

    let popover = this.popoverCtrl.create(PopoverCredito, {
      credito: this.vCredito
    })

    popover.present({
      ev: event
    });

    popover.onDidDismiss((data) => {

    })

  }

  openDetalhe() {
    this.alertCtrl.create({
      message: '',
      buttons: ['OK']
    }).present();
  }
}
