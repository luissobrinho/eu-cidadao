import { SorteioPage } from './../sorteio/sorteio';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StatusBar } from "@ionic-native/status-bar";

/**
 * Generated class for the NotificacaoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notificacao',
  templateUrl: 'notificacao.html',
})
export class NotificacaoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public statusBar: StatusBar) {
  }

  ionViewDidEnter() {
    this.statusBar.backgroundColorByHexString('#FBC02D');
  }

  openSorteio() {
    this.navCtrl.push(SorteioPage);
  }

}
