import { SorteioPage } from './../sorteio/sorteio';
import { NotasPage } from './../notas/notas';
import { StatusBar } from '@ionic-native/status-bar';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ConsultaPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-consulta',
  templateUrl: 'consulta.html',
})
export class ConsultaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public statusBar: StatusBar) {
  }

  ionViewDidEnter(){
    this.statusBar.backgroundColorByHexString('#1976D2');
  }

  openNotas() {
    this.navCtrl.push(NotasPage)
  }

  openSorteio() {
    this.navCtrl.push(SorteioPage);
  }

}
