import { DenunciaProvider } from './../../providers/denuncia/denuncia';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StatusBar } from '@ionic-native/status-bar';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

/**
 * Generated class for the DenunciaPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-denuncia',
  templateUrl: 'denuncia.html',
})
export class DenunciaPage {

  denunciaFrom: FormGroup;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public statusBar: StatusBar,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public denunciaProvader: DenunciaProvider
  ) {
    this.denunciaFrom = new FormGroup({
      cNF: new FormControl(''),
      cnpjDestinatario: new FormControl(''),
      cnpjEmitente: new FormControl('', Validators.required),
      cpfDestinatario: new FormControl(''),
      dataEmissao: new FormControl('', Validators.required),
      denuncia: new FormControl('', Validators.required),
      numeroECF: new FormControl('', Validators.required),
      serie: new FormControl(''),
      situacao: new FormControl('', Validators.required),
      subSerie: new FormControl(''),
      tipoDenuncia: new FormControl('1', Validators.required),
      tipoDocumento: new FormControl('NF-E', Validators.required),
      valor: new FormControl('', Validators.required),
    });
  }

  ionViewDidEnter() {
    this.statusBar.backgroundColorByHexString('#7B1FA2');
  }

  onSubmit() {
    console.log(this.denunciaFrom.value);
    let loading = this.loadingCtrl.create({
      content: 'Aguarde...',
      duration: 2000
    });
    loading.present()
    loading.onDidDismiss(() => {
      this.denunciaProvader.incluirDenuncia(this.denunciaFrom.value).subscribe((res) => {
        if(res.codigo === 200) {
          this.denunciaFrom.reset();
          this.alertCtrl.create({
            message: 'Sua denuncia foi para SEFAZ, ele tomará o frente para você!',
            buttons: [{
              text: 'OK',
              handler: () => {
                this.navCtrl.pop();
              }
            }]
          }).present();
        } else if (res.codigo === 401) {
          this.alertCtrl.create({
            message: 'Usuário não autorizado a consultar esta função',
            buttons: ['OK']
          }).present();
        } else if (res.codigo === 403) {
          this.alertCtrl.create({
            message: 'Usuário sem acesso',
            buttons: ['OK']
          }).present();
        } else if(res.codigo === 404){
          this.alertCtrl.create({
            message: 'Usuário não encontrado',
            buttons: ['OK']
          }).present();
        }
      })
    });
  }
}
