import { AlterarSenhaProvider } from './../../providers/alterar-senha/alterar-senha';
import { StatusBar } from '@ionic-native/status-bar';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-alterar-senha',
  templateUrl: 'alterar-senha.html',
})
export class AlterarSenhaPage {
  alterarSenhaForm: FormGroup;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public statusBar: StatusBar,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public asProvider: AlterarSenhaProvider
  ) {
    this.alterarSenhaForm = new FormGroup({
      fraseEmail: new FormControl('', Validators.required),
      resposta: new FormControl('', Validators.required),
      login: new FormControl('', [Validators.minLength(11), Validators.required]),
      senhaAntiga: new FormControl('', [Validators.minLength(6), Validators.required]),
      senhaNova: new FormControl('', [Validators.minLength(6), Validators.required]),
      senhaNovaConfirm: new FormControl('', [Validators.minLength(6), Validators.required]),
    }, this.senhaMatchValidator);
  }

  ionViewDidEnter() {
    this.statusBar.backgroundColorByHexString('#D32F2F');
  }

  senhaMatchValidator(g: FormGroup) {
    return g.get('senhaNova').value === g.get('senhaNovaConfirm').value
      ? null : { 'mismatch': true };
  }

  onSubmit() {
    console.log(this.alterarSenhaForm.value);
    let loading = this.loadingCtrl.create({
      content: 'Aguarde...',
      duration: 2000
    });
    loading.present()
    this.alterarSenhaForm.removeControl('senhaNovaConfirm');
    this.asProvider.alterarSenha(this.alterarSenhaForm.value)
    loading.onDidDismiss(() =>{

      this.alterarSenhaForm.reset();
      this.alertCtrl.create({
        title: 'Alteração de Senha',
        message: 'Sua senha foi alterada com sucesso!',
        buttons: [{
          text: 'OK',
          handler: () => {
            this.navCtrl.pop();
          }
        }]
      }).present();
    });
  }
}
