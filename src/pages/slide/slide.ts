import { LoginPage } from './../login/login';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';

/**
 * Generated class for the SlidePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-slide',
  templateUrl: 'slide.html',
})
export class SlidePage {
  slides: {}[]
  @ViewChild(Slides) slidesView: Slides;
  currentIndex: number = 0;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.slides = [
      {
        title: "Eu Cidadão",
        description: "Uma maneira inteligente de participar",
        image: "assets/img/icon-pmartphone-e-mao.png",
      },
      {
        title: "Denúncias",
        description: "Seja ainda mais inteligente, participe, fiscalize e comunique",
        image: "assets/img/megaphone.png",
      },
      {
        title: "Sorteio",
        description: "Além de participar, que tal ganhar prêmios?",
        image: "assets/img/sorteio.png",
      },
      {
        title: "Jogue",
        description: "Participe brincando, ganhe ainda mais e faça cidadania",
        image: "assets/img/joystick.png",
      }
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SlidePage');
  }

  next() {
    this.slidesView.slideNext();
  }

  slideChanged() {
    this.currentIndex = this.slidesView.getActiveIndex();
  }

  openLogin() {
    this.navCtrl.push(LoginPage);
  }

  pular() {
    this.slidesView.slideTo(3);
  }
}
