import { StatusBar } from '@ionic-native/status-bar';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { DenunciaProvider } from "../../providers/denuncia/denuncia";

/**
 * Generated class for the AutoDenunciaPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-auto-denuncia',
  templateUrl: 'auto-denuncia.html',
})
export class AutoDenunciaPage {

  
  denunciaFrom: FormGroup;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public statusBar: StatusBar,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public denunciaProvader: DenunciaProvider
  ) {
    this.denunciaFrom = new FormGroup({
      cNF: new FormControl(''),
      cnpjDestinatario: new FormControl(''),
      cnpjEmitente: new FormControl('', Validators.required),
      cpfDestinatario: new FormControl(''),
      dataEmissao: new FormControl('', Validators.required),
      denuncia: new FormControl('', Validators.required),
      numeroECF: new FormControl('', Validators.required),
      serie: new FormControl(''),
      situacao: new FormControl('', Validators.required),
      subSerie: new FormControl(''),
      tipoDenuncia: new FormControl('1', Validators.required),
      tipoDocumento: new FormControl('NF-E', Validators.required),
      valor: new FormControl('', Validators.required),
    });
  }

  ionViewDidEnter() {
    this.statusBar.backgroundColorByHexString('#7B1FA2');
  }

  onSubmit() {
    console.log(this.denunciaFrom.value);
    let loading = this.loadingCtrl.create({
      content: 'Aguarde...',
      duration: 2000
    });
    loading.present()
    loading.onDidDismiss(() => {
      window.localStorage.setItem(this.denunciaFrom.controls['numeroECF'].value, JSON.stringify(this.denunciaFrom.value))
      this.denunciaFrom.reset();
    });
  }
}
