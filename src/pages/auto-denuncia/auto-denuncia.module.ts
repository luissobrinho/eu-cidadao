import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AutoDenunciaPage } from './auto-denuncia';

@NgModule({
  declarations: [
    AutoDenunciaPage,
  ],
  imports: [
    IonicPageModule.forChild(AutoDenunciaPage),
  ],
})
export class AutoDenunciaPageModule {}
