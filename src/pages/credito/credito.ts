import { ConsultaEntidadeProvider } from './../../providers/consulta-entidade/consulta-entidade';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StatusBar } from '@ionic-native/status-bar';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Entidade } from "../../models/entidade.model";
import { SolicitarAutenticarProvider } from "../../providers/solicitar-autenticar/solicitar-autenticar";

/**
 * Generated class for the CreditoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-credito',
  templateUrl: 'credito.html',
})
export class CreditoPage {

  doarFrom: FormGroup;
  sacarFrom: FormGroup
  entidades: Entidade[]
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public statusBar: StatusBar,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public entidadeProvider: ConsultaEntidadeProvider,
    public saProvider: SolicitarAutenticarProvider
  ) {
    this.doarFrom = new FormGroup({
      id: new FormControl('', Validators.required),
      cpf: new FormControl('', [Validators.required, Validators.minLength(11), Validators.maxLength(11)])
    });

    this.doarFrom.controls['cpf'].setValue(this.saProvider.getCpf());
    this.entidadeProvider.getEntidades()
      .subscribe((entidades: Entidade[]) => {
        this.entidades = entidades;
      })

  }

  ionViewDidEnter() {
    this.statusBar.backgroundColorByHexString('#F57C00');
  }

  onSubmit() {
    console.log(this.doarFrom.value);
    let loading = this.loadingCtrl.create({
      content: 'Aguarde...',
      duration: 2000
    });
    loading.present()
    loading.onDidDismiss(() => {
      this.entidadeProvider.adotarEntidade(this.doarFrom.value)
      this.doarFrom.reset();
      this.alertCtrl.create({
        title: 'Obrigado!',
        message: 'A instituição receberá sua doação.',
        buttons: [{
          text: 'OK',
          handler: () => {
            this.navCtrl.pop();
          }
        }]
      }).present();
    });
  }

}
