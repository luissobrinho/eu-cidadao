import { StatusBar } from '@ionic-native/status-bar';
import { HomePage } from './../home/home';
import { SolicitarAutenticar } from './../../models/solicitarAutenticar.model';
import { SolicitarAutenticarProvider } from './../../providers/solicitar-autenticar/solicitar-autenticar';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, App, LoadingController, Loading } from 'ionic-angular';
import { Device } from '@ionic-native/device';
import { InAppBrowser, InAppBrowserObject, InAppBrowserEvent } from "@ionic-native/in-app-browser";
import { AutenticacaoAutenticar } from "../../models/autenticacaoAutenticar.model";


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  sa: SolicitarAutenticar;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public saProvider: SolicitarAutenticarProvider,
    public device: Device,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    private iab: InAppBrowser,
    private app: App,
    public loadingCtrl: LoadingController,
    public statusBar: StatusBar
  ) {
    this.sa = {
      login: '',
      nomeDispositivo: this.device.model,
      tokenApp: '5a871b44b391796edaf66a0005c8492fcf16a7e6'
    }
  }

  ionViewDidEnter(){
    this.statusBar.backgroundColorByHexString('#E6333A');
  }

  setSolicitar(): void {
    if (this.sa.login.length === 11) {
      let loading: Loading = this.loadingCtrl.create({
        content: 'Aguarde...',
        duration: 2000
      });
      loading.present();
      loading.onDidDismiss(() => {
        let id = this.saProvider.getIdAutenticacao();
        console.log('this.saProvider.getIdAutenticacao()', id);
        if (id === null) {
          this.saProvider.solicitar(this.sa)
            .then((autorizado) => {
              console.log('solicitar, then', autorizado);
              if (autorizado) {
                this.saProvider.saveIdAutenticacao(autorizado, this.sa.login);
                this.alertCtrl.create({
                  title: 'Seu CPF foi encontrado!',
                  message: 'Agora autorize a liberação do aplicativo para acessar seus dados, a liberação e feita acessando o site: http://hackathonhabilitacao.sefaz.al.gov.br/',
                  buttons: [{
                    text: 'Autorizar',
                    handler: () => {
                      let iab: InAppBrowserObject = this.iab
                        .create('http://hackathonhabilitacao.sefaz.al.gov.br/',
                        '_blank',
                        `zoom=no,MediaPlaybackRequiresUserAction=no,location=yes,shouldPauseOnSuspend=yes`);
                      iab.show();
                      //iab.on('exit').subscribe((iabE: InAppBrowserEvent) => {
                      //})
                    }
                  }]
                })
                  .present();

              }
            }).catch((e) => {
              console.log('solicitar, catch', e);

              this.alertCtrl
                .create({
                  title: 'Ops...',
                  message: JSON.parse(e._body).mensagem || 'Houve um erro inesperado!',
                  buttons: ['OK']
                })
                .present();
            })
        } else {
          let aa: AutenticacaoAutenticar = { login: this.sa.login, idAutorizacao: id, tokenApp: this.sa.tokenApp };
          this.saProvider.autenticacao(aa).then((autenticacao) => {
            console.log('autenticacao then', autenticacao);

            this.app.getActiveNav().setRoot(HomePage).then(() => {
              this.saProvider.setToken(autenticacao.id_token);
            });
          }).catch((erro) => {
            console.log('autenticacao catch', erro);
            this.alertCtrl
              .create({
                title: 'Ops...',
                message: JSON.parse(erro._body).mensagem || 'Houve um erro inesperado!',
                buttons: ['ok']
              })
              .present();
          });
        }
      })
    } else {
      this.toastCtrl.create({
        message: 'CPF Inválido',
        position: 'top',
        duration: 3500
      }).present();
    }
  }

}
