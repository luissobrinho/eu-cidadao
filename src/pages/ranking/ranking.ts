import { PerfilPage } from './../perfil/perfil';
import { FirebaseAuthState } from 'angularfire2';
import { AngularFire } from 'angularfire2';
import { StatusBar } from '@ionic-native/status-bar';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RankingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ranking',
  templateUrl: 'ranking.html',
})
export class RankingPage {
  user: firebase.UserInfo;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public statusBar: StatusBar,
    public af: AngularFire
  ) {
    this.user = {
      displayName: '',
      email: '',
      photoURL: '',
      providerId: '',
      uid: ''
    };
    this.af.auth.subscribe((state: FirebaseAuthState) => {
      if(state !== null) {
        this.user = state.google;
      }
    });
  }

  ionViewDidEnter() {
    this.statusBar.backgroundColorByHexString('#388E3C');
  }

  openPerfil(user: firebase.UserInfo) {
    this.navCtrl.push(PerfilPage, {user: user})
  }

}
