export class Notas {
    dataEmissao: string;
    numeroNotaFiscal: string;
    tipoNotaFiscal: string;
    valorTotal: string;
    descricaoContribuinte: string;
    descricaoMotivoAnulacao: string;
    numeroEmitente: string;
    valorCredito: string;

    constructor(
        dataEmissao: string,
        numeroNotaFiscal: string,
        tipoNotaFiscal: string,
        valorTotal: string,
        descricaoContribuinte: string,
        descricaoMotivoAnulacao: string,
        numeroEmitente: string,
        valorCredito: string
    ) {

    }
}