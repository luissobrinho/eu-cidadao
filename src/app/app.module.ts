import { AutoDenunciaPage } from './../pages/auto-denuncia/auto-denuncia';
import { SlidePage } from './../pages/slide/slide';
import { PerfilPage } from './../pages/perfil/perfil';
import { RankingPage } from './../pages/ranking/ranking';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from './../pages/login/login';
import { HeaderAppComponent } from './../components/header-app/header-app';
import { ConsultaPage } from './../pages/consulta/consulta';
import { AlterarSenhaPage } from './../pages/alterar-senha/alterar-senha';
import { PopoverCredito } from './../pages/notas/popoverCredito';
import { NotasPage } from './../pages/notas/notas';
import { SorteioPage } from './../pages/sorteio/sorteio';
import { PopoverConfigSorteio } from './../pages/sorteio/popoverConfigSorteio';
import { NotificacaoPage } from './../pages/notificacao/notificacao';
import { DenunciaPage } from './../pages/denuncia/denuncia';
import { EducacaoPage } from './../pages/educacao/educacao';
import { CreditoPage } from './../pages/credito/credito';
import { GamePage } from './../pages/game/game';

import { AdotarProvider } from '../providers/adotar/adotar';
import { AlterarDenunciaProvider } from '../providers/alterar-denuncia/alterar-denuncia';
import { AlterarSenhaProvider } from '../providers/alterar-senha/alterar-senha';
import { BilheteProvider } from '../providers/bilhete/bilhete';
import { BilheteContempladoProvider } from '../providers/bilhete-contemplado/bilhete-contemplado';
import { ConsultaCreditoProvider } from '../providers/consulta-credito/consulta-credito';
import { ConsultaEntidadeProvider } from '../providers/consulta-entidade/consulta-entidade';
import { DenunciaProvider } from '../providers/denuncia/denuncia';
import { IncluirDenunciaProvider } from '../providers/incluir-denuncia/incluir-denuncia';
import { NotaProvider } from '../providers/nota/nota';
import { SerteioProvider } from '../providers/serteio/serteio';
import { SolicitarAutenticarProvider } from '../providers/solicitar-autenticar/solicitar-autenticar';

import { HttpModule } from "@angular/http";
import { NativeStorage } from "@ionic-native/native-storage";
import { Device } from "@ionic-native/device";
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AngularFireModule, FirebaseAppConfig } from 'angularfire2';
import { AngularFireAuth } from "angularfire2/auth";

const firebaseConfig: FirebaseAppConfig = {
  apiKey: "AIzaSyDsLtSx23BryQq-CHuTwRsoL8ICezqZ930",
  authDomain: "eucidadao-ffd58.firebaseapp.com",
  databaseURL: "https://eucidadao-ffd58.firebaseio.com",
  storageBucket: "eucidadao-ffd58.appspot.com",
  messagingSenderId: "20570747613"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    HeaderAppComponent,
    ConsultaPage,
    AlterarSenhaPage,
    NotasPage,
    PopoverCredito,
    SorteioPage,
    PopoverConfigSorteio,
    NotificacaoPage,
    DenunciaPage,
    EducacaoPage,
    CreditoPage,
    GamePage,
    RankingPage,
    PerfilPage,
    SlidePage,
    AutoDenunciaPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    ConsultaPage,
    AlterarSenhaPage,
    NotasPage,
    PopoverCredito,
    SorteioPage,
    PopoverConfigSorteio,
    NotificacaoPage,
    DenunciaPage,
    EducacaoPage,
    CreditoPage,
    GamePage,
    RankingPage,
    PerfilPage,
    SlidePage,
    AutoDenunciaPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AdotarProvider,
    AlterarDenunciaProvider,
    AlterarSenhaProvider,
    BilheteProvider,
    BilheteContempladoProvider,
    ConsultaCreditoProvider,
    ConsultaEntidadeProvider,
    DenunciaProvider,
    IncluirDenunciaProvider,
    NotaProvider,
    SerteioProvider,
    SolicitarAutenticarProvider,
    NativeStorage,
    Device,
    InAppBrowser,
    AngularFireAuth
  ]
})
export class AppModule {}
