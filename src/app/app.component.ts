import { AutoDenunciaPage } from './../pages/auto-denuncia/auto-denuncia';
import { SlidePage } from './../pages/slide/slide';
import { CreditoPage } from './../pages/credito/credito';
import { GamePage } from './../pages/game/game';
import { DenunciaPage } from './../pages/denuncia/denuncia';
import { ConsultaPage } from './../pages/consulta/consulta';
import { FirebaseAuthState } from 'angularfire2';
import { AngularFire } from 'angularfire2';
import { AlterarSenhaPage } from './../pages/alterar-senha/alterar-senha';
import { SolicitarAutenticarProvider } from './../providers/solicitar-autenticar/solicitar-autenticar';
import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import * as firebase from 'firebase';

import { HomePage } from '../pages/home/home';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;

  @ViewChild('content') nav: Nav;

  user: firebase.UserInfo;
  showMenu: boolean = false;
  menuItem: Array<{ title: string, icon: string, component: any }>;
  menuItemOuter: Array<{ title: string, icon: string, f: any }>;
  constructor(
    public saProvider: SolicitarAutenticarProvider,
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public af: AngularFire,
    public alertCtrl: AlertController
  ) {

    this.user = {
      displayName: '',
      email: '',
      photoURL: '',
      providerId: '',
      uid: ''
    }

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      console.log('saProvider.getToken()', saProvider.getToken());
      this.menuItem = [{
        title: 'Principal',
        icon: 'home',
        component: HomePage
      }
        ,
      {
        title: 'Consulta',
        icon: 'search',
        component: ConsultaPage
      }
        ,
      {
        title: 'Denuncia',
        icon: 'warning',
        component: DenunciaPage
      }
        ,
      {
        title: 'Denuncia Automática',
        icon: 'send',
        component: AutoDenunciaPage
      }
        ,
      {
        title: 'Game',
        icon: 'basketball',
        component: GamePage
      }
        ,
      {
        title: 'Utilização de Créditos',
        icon: 'card',
        component: CreditoPage
      }
        ,
      {
        title: 'Alterar Senha',
        icon: 'key',
        component: AlterarSenhaPage
      }
        ,
      {
        title: 'Educação',
        icon: 'list-box',
        component: AlterarSenhaPage
      }
      ];

      this.menuItemOuter = [
        {
          title: 'Contato',
          icon: 'contact',
          f: () => {
            this.alertCtrl.create({
              title: 'Contato',
              message: `<b>Endereço:</b> R. Gen. Hermes, 80 - Cambona, Maceió - AL, 57017-900<br>
              <b>Telefone:</b> (82) 3315-9000`,
              buttons: ['Fechar']
            }).present()
          }
        }
      ];
      (saProvider.getToken() !== null) ? this.rootPage = HomePage : this.rootPage = SlidePage;
      this.af.auth.subscribe((state: FirebaseAuthState) => {
        if(state !== null) {
          this.user = state.google;
          this.showMenu = true;
        }
      });

      statusBar.overlaysWebView(true);

      window.setTimeout(() => {
        splashScreen.hide();
      }, 5000);
    });
  }

  openPage(item) {
    console.log(this.nav.getActive());
    if (this.nav.getActive().component !== item.component) {
      this.nav.push(item.component);
    }
  }

  openOption(itemO) {
    itemO.f();
  }
}

